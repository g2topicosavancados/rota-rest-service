const Trip = require("../models/tripModel.js");
const mongoose = require('mongoose');

const tripServiceId = async (id) => {
    if(id){
    const trip = await Trip.find({"idTrip": id});
    return {code: 200, trip}
}
  else{
      return { code: 404, error: "Id not found" };
  }
};



module.exports = tripServiceId;
