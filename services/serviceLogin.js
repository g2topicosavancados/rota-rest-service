const Driver = require("../models/driverModel.js");
const Passenger = require("../models/passengerModel.js");

const loginService = async (email, password) => {
  let userFound;

  const isDriver = await Driver.findOne({ email });
  const isPassenger = await Passenger.findOne({ email });

  userFound = isDriver || isPassenger || null;
  //User exists case
  if (userFound) {
    const userType = isDriver ? "driver": "passenger"
    const canLogin = await userFound.compareHash(password);
    //User can login case
    if (canLogin) {
      const token = userFound.generateToken();
      const responseUser = { email, token, userType }; //Inclui a variavel userType no responseUser (Return)
      return { responseUser, code: 200 };
    }
    //User can't login case
    return { error: "Password wrong", code: 401 };
  }
  //User not found
  return { error: "User not found" };
};

module.exports = loginService;

