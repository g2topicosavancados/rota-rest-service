const Trip = require("../models/tripModel.js");
const mongoose = require('mongoose');

const editArriveTimeService = async (idTrip, email, horario) => {

    if(idTrip){
        const trip = await Trip.findOne({"idTrip": idTrip});
        //console.log(trip, "TESTE")
        const newPassengers = trip.passengers.map(passenger=> {
            if (passenger.email===email){
                passenger.horario = horario
            }    
            return passenger
        })
        //console.log(newPassengers, "TESTETESTE")     
        trip.passengers = newPassengers
        await trip.save(trip);
        return { code: 200, trip }
    }
    else {
        return { code: 404, error: "Id not found" };
    }



}

module.exports = editArriveTimeService;