const Driver = require('../models/driverModel.js');

const driverService = async (termo) => {
    if(termo){
        const drivers = await Driver.find({$or:[{name: termo}, {destino: termo}]}); //ainda não existe o destino no db mas pode colocar qualquer coluna existente
        return {code: 200, drivers}
    }
    else{
        return { code: 404, error: "Term not found" };
    }
}

module.exports = driverService;