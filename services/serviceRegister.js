const Driver = require("../models/driverModel.js");
const Passenger = require("../models/passengerModel.js");

const registerService = async (newUserData) => {
  const { type } = newUserData;

  switch (type) {
    case "driver":
      return await addNewUser(newUserData, Driver, "driver");

    case "passenger":
      return await addNewUser(newUserData, Passenger, "passenger");

    default:
      return { error: "Type sent does not match" };
  }
};

const addNewUser = async (data, PersistenceType, userType) => {
  const { name, email, password, phone } = data;

  if (name && email && password && phone) {
    const user = new PersistenceType({ name, email, password, phone });
    await user.save(user);
    const token = user.generateToken();
    const responseUser = { email, token, userType};

    return { responseUser, code: 200 };
  }
};

module.exports = registerService;
