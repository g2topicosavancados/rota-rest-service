const Trip = require ("../models/tripModel.js");

const tripService = async (tripData) => {
  const { description, address, driverEmail } = tripData;

  const idTrip = await geraId();

  if (description && address && driverEmail) {
    try {
      const trip = new Trip({ description, idTrip, address, driverEmail}); 
      await trip.save(trip);
      return { response: { idTrip }, code: 200 };
    } catch (e) { 
      console.log("Server error: ", e); 
      return { code: 500, response: "Server error, cannot create a trip!" }
    }
  }
};

const geraId = async () => {

  let randomId = Math.floor(Math.random() * (1000000 - 100000) + 100000);
  try {
    const searchResult = await Trip.findOne({ "idTrip": randomId  })
    return searchResult ? geraId : randomId ;
  } catch (error) { 
    console.log("Erro ao consultar banco: ", error)
  }

}

module.exports = tripService;