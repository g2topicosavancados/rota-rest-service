const Trip = require("../models/tripModel.js");
const mongoose = require('mongoose');

const AddPassengerTripService = async (idTrip, email) => {

    if(idTrip){
        const trip = await Trip.findOne({"idTrip": idTrip});
        const passenger = {

                "email": email,
                "addressBoarding": {
                  "latitude": "",
                  "longitude": "",
                  "pending": "false"
                },
                "addressDisembarkation": {
                  "latitude":"",
                  "longitude":"",
                  "pending": "false"
                }
        }
        trip.passengers.push(passenger);
        await trip.save(trip);
        return {code: 200, trip}
    }
      else{
          return { code: 404, error: "Id not found" };
      }



}

module.exports = AddPassengerTripService;