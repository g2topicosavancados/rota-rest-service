const mongoose = require('mongoose');
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { Schema } = require('mongoose');[]

const passengerSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true    
    },
    trips: Array,
    phone: String,
},
{ timestamps: true });

passengerSchema.pre("save", async function hashPassword(next) {
    if (!this.isModified("password")) next();
    this.password = await bcrypt.hash(this.password, 8);
});
  
passengerSchema.methods = {
    compareHash(hash) {
        console.log('Entrei no compare: ', hash);
        return bcrypt.compare(hash, this.password)
    },

    generateToken() {
        return jwt.sign({ id: this.id }, "secret", {
        expiresIn: 86400});
    }
}

module.exports = mongoose.model('Passenger', passengerSchema);