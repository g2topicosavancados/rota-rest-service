const mongoose = require('mongoose');
const { Schema } = require('mongoose');

const tripSchema = new Schema({
    description: {
        type: String,
        required: true,
    },
    idTrip: {
        type: String,
        required: true,
    },
    address: {
        type: Object,
        required: true,
    },
    driverEmail: {
        type: String,
        required: true,
    },
    passengers: {
        type: Array,
        required: true,
    }
},
{ timestamps: true });

module.exports = mongoose.model('Trip', tripSchema, 'trip');

  