let router = require("express").Router();
const authMiddleware = require("../middlewares/auth");
const loginService = require("../services/serviceLogin");
const registerService = require("../services/serviceRegister");
const tripService = require("../services/serviceTrip");
const tripServiceId = require("../services/serviceTripId");
const editArriveTimeService = require("../services/serviceEditArrive")

const AddPassengerTripService = require("../services/serviceAddPassengerTrip")

router.post("/register", async (req, res) => {
  const { responseUser, code } = await registerService(req.body);

  return res.status(code).send(responseUser);
});

router.post("/login", async (req, res) => {
  const { email, password } = req.body;
  const { responseUser, error, code } = await loginService(email, password);

  return res.status(code).send(responseUser || error);
});

router.use(authMiddleware);

router.post("/AddPassengerTrip", async (req, res) => {
  const { idTrip, email } = req.body;
  const {trip, code, error} = await AddPassengerTripService(idTrip, email);
  return res.status(code).send(trip || error);
});

router.post("/EditArriveTime", async (req, res) => {
  const { idTrip, email, horario} = req.body;
  const {trip, code, error} = await editArriveTimeService(idTrip, email, horario);
  return res.status(code).send(trip || error);
});

//Status of address request
router.get("/trip/:idTrip/:email", async (req, res) => {
  const { idTrip, email } = req.params;
  const {responseStatus, code} = await tripStatusService(idTrip, email);

  return res.status(code).send({responseStatus});
});

router.get('/driver', async (req, res) => {
  const {drivers, code, error} = await serviceDriver(req.body.termo);
  return res.status(code).send(drivers || error);
});

//Add a new trip by driver
router.post("/trip", async (req, res) => {
  const { response, code } = await tripService(req.body);
  return res.status(code).send(response);
});

router.get('/trip/:id', async (req, res) => {
  const {trip, code, error} = await tripServiceId(req.params.id);
  return res.status(code).send(trip || error);
});

module.exports = router;