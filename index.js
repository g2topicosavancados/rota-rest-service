const express = require("express");
const app = express(); // Initialize the app
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const apiRoutes = require("./controllers/routes");
const dotenv = require("dotenv").config();

app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use(bodyParser.json());

mongoose
  .connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Successfully connected to the database");
  })
  .catch((err) => {
    console.log("Could not connect to the database. Exiting now...", err);
    process.exit();
  });

var port = process.env.PORT || 5000; //Setup server port

app.get("/", (_, res) => res.send("Hello World with Express"));
app.use("/api", apiRoutes);

app.listen(port, function () {
  console.log("api ROTA rodando na porta  " + port);
});

module.exports = app;
